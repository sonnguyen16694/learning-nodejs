console.log("CLient side javascript file is loaded!");

const weatherForm = document.querySelector("form");
const searchInput = document.querySelector("input");
const messageOne = document.querySelector("#messageOne");
const messageTwo = document.querySelector("#messageTwo");
weatherForm.addEventListener("submit", (e) => {
  e.preventDefault();
  messageOne.textContent = "";
  messageTwo.textContent = "loading";
  fetch(`http://localhost:3000/weather?address=${searchInput.value}`).then(
    (response) => {
      response.json().then((data) => {
        if (data.error) {
          messageOne.textContent = data.error;
        } else {
          messageOne.textContent = data.location;
          messageTwo.textContent = data.forecast;
        }
      });
    }
  );
});
