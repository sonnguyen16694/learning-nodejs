const express = require("express");
const path = require("path");
const hbs = require("hbs");
const geoRequest = require("./utils/geocode");
const forecast = require("./utils/forecast");

const app = express();

//defind path for express config
const publicDirectoryPath = path.join(__dirname, "../public");
const viewsPath = path.join(__dirname, "../templates/views");
const partialsPath = path.join(__dirname, "../templates/partials");

//setup handlebars engine and views location
app.set("view engine", "hbs");
app.set("views", viewsPath);
hbs.registerPartials(partialsPath);

//setup static directory to serve
app.use(express.static(publicDirectoryPath));

app.get("", (req, res) => {
  res.render("index", {
    title: "Weather",
    name: "Son",
  });
});
app.get("/help", (req, res) => {
  res.render("help", {
    title: "Help",
    message: "This is help page",
    name: "Son",
  });
});
app.get("/about", (req, res) => {
  res.render("about", {
    title: "About",
    name: "Son",
  });
});
app.get("/weather", (req, res) => {
  if (!req.query.address) {
    return res.send({
      error:
        "Add address property onto JSON which returns the provided address",
    });
  }
  geoRequest(req.query.address, (error, { long, lat, location } = {}) => {
    if (!error) {
      forecast(long, lat, (forecastError, forecastData) => {
        if (forecastError) {
          return res.send({
            error: forecastError,
          });
        } else {
          return res.send({
            forecast: forecastData,
            location: location,
          });
        }
      });
    } else {
      return res.send({
        error: error,
      });
    }
  });
});

app.get("/products", (req, res) => {
  if (!req.query.search) {
    return res.send({
      error: "You must provide a search term",
    });
  }
  console.log(req.query);
  res.send({
    products: [],
  });
});

app.get("/help/*", (req, res) => {
  res.render("404", {
    title: "Not found",
    errorMessage: "Help article not found",
    name: "Son Nguyen Ngoc",
  });
});

app.get("*", (req, res) => {
  res.render("404", {
    title: "Not found",
    errorMessage: "Page not found",
    name: "Son Nguyen Ngoc",
  });
});

app.listen(3000, () => {
  console.log("Server is up on port 3000.");
});
