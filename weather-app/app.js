const request = require("request");
const geoRequest = require("./utils/geocode");
const forecast = require("./utils/forecast");
if (process.argv.length > 2) {
  geoRequest(process.argv[2], (error, { long, lat, location } = {}) => {
    if (!error) {
      forecast(long, lat, (forecastError, forecastData) => {
        if (forecastError) {
          console.log("Error", forecastError);
        } else {
          console.log(location);
          console.log(forecastData);
        }
      });
    } else {
      console.log("Error", error);
    }
  });
} else {
  console.log("Please provide an address.");
}
