const request = require("request");
const forecast = (long, lat, callback) => {
  const url = `http://api.weatherstack.com/current?access_key=a1f25ef598a57face17afec317f1beed&query=${lat},${long}`;
  request(
    {
      url: url,
      json: true,
    },
    (error, { body }) => {
      if (error) {
        callback("Cannot connect to weather api.", undefined);
      } else if (body.success === false) {
        callback(body.error.info, undefined);
      } else {
        //console.log(response.body.current);
        const current = body.current;
        callback(
          undefined,
          current.weather_descriptions[0] +
            ". It is currently " +
            current.temperature +
            " degress out. It feels like " +
            current.feelslike +
            " degress out."
        );
      }
    }
  );
};

module.exports = forecast;
