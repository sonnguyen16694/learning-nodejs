const request = require("request");
const geoRequest = (address, callback) => {
  const url_geo_api = `https://api.mapbox.com/geocoding/v5/mapbox.places/${address}.json?access_token=pk.eyJ1IjoidHV5a2llbXRpZW4iLCJhIjoiY2tqNzUzM3BlMWF6cDJwbnZ0OHM0NWRpcSJ9.TaVZ5F9_ZZthhwSmbu6eCQ`;
  request(
    {
      url: url_geo_api,
      json: true,
    },
    (error, { body }) => {
      if (error) {
        callback("Cannot connect to geolocation api.", undefined);
      } else if (body.message) {
        callback(message, undefined);
      } else if (body.features.length === 0) {
        callback("Unable to find location. Try another search.", undefined);
      } else {
        callback(undefined, {
          lat: body.features[0].center[1],
          long: body.features[0].center[0],
          location: body.features[0].place_name,
        });
      }
    }
  );
};

module.exports = geoRequest;
