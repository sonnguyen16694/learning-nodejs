const fileName = "notes.json";
const fs = require("fs");
const chalk = require("chalk");
const getNotes = function () {
  const notes = loadNotes();
  console.log(chalk.inverse("Your notes"));
  notes.forEach((note) => {
    console.log(note.title);
  });
};

const addNotes = (title, body) => {
  const notes = loadNotes();
  const note = notes.find((s) => s.title === title);
  if (!note) {
    notes.push({
      title: title,
      body: body,
    });
    saveNotes(notes);
    console.log(chalk.green("New note added!"));
  } else {
    console.error(chalk.red("Duplicate note"));
  }
};
const removeNotes = (title) => {
  const notes = loadNotes();
  const removeNoteIndex = notes.findIndex((s) => s.title === title);
  if (removeNoteIndex > -1) {
    notes.splice(removeNoteIndex, 1);
    saveNotes(notes);
    console.log(chalk.green("Remove note successful!"));
  } else {
    console.log(chalk.red("Not found note."));
  }
};

const readNote = (title) => {
  const notes = loadNotes();
  const note = notes.find((s) => s.title === title);
  if (note) {
    console.log(chalk.inverse(note.title));
    console.log(note.body);
  } else {
    console.log(chalk.red("No result found."));
  }
};

const loadNotes = () => {
  try {
    const dataBuffer = fs.readFileSync(fileName);
    const dataJson = dataBuffer.toString();
    return JSON.parse(dataJson);
  } catch (e) {
    return [];
  }
};
const saveNotes = (data) => {
  fs.writeFileSync(fileName, JSON.stringify(data));
};
module.exports = {
  getNotes: getNotes,
  addNotes: addNotes,
  removeNotes: removeNotes,
  readNote: readNote,
};
