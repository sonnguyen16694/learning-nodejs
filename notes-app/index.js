const chalk = require("chalk");
const yargs = require("yargs");
const notes = require("./notes");

//Customzie yargs version
yargs.version("1.1.0");

//add, remove, read, list
//create add command
yargs.command(
  "add",
  "Add new note",
  {
    title: {
      describe: "Note title",
      demandOption: true,
      type: "string",
    },
    body: {
      describe: "Note body",
      demandOption: true,
      type: "string",
    },
  },
  (argv) => {
    notes.addNotes(argv.title, argv.body);
  }
);
//create remove command
yargs.command(
  "remove",
  "Remove a note",
  {
    title: {
      describe: "Note title",
      demandOption: true,
      type: "string",
    },
  },
  (argv) => {
    notes.removeNotes(argv.title);
  }
);
//create read command
yargs.command(
  "read",
  "Read a note",
  {
    title: {
      describe: "Title of a note",
      demandOption: true,
      type: "string",
    },
  },
  (argv) => {
    notes.readNote(argv.title);
  }
);
//create view all command
yargs.command("list", "List notes", () => {
  notes.getNotes();
});
yargs.parse();
