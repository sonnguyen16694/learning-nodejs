//object property shorthand
const name = "Son";
const userAge = 27;

const user = {
  name,
  age: userAge,
  location: "Hanoi",
};

// console.log(user);

//object destructuring
const product = {
  label: "Red note book",
  price: 3,
  stock: 201,
  salePrice: undefined,
};

// const { label: productLabel, stock } = product;

// console.log(productLabel, stock);

const transaction = (type, { label, stock }) => {
  console.log(type, label, stock);
};
transaction("order", product);
