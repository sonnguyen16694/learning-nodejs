// setTimeout(() => {
//   console.log("2 seconds are up.");
// }, 2000);

// const names = ["Son", "Jane", "Anna"];
// const shortName = names.filter((name) => {
//   return name.length < 4;
// });

// const geoRequest = (address, callback) => {
//   setTimeout(() => {
//     const data = {
//       lat: 0,
//       log: 0,
//     };
//     callback(data);
//   }, 2000);
// };
// geoRequest("Hanoi", (data) => {
//   console.log(data);
// });

const add = (a, b, callback) => {
  setTimeout(() => {
    callback(a + b);
  }, 2000);
};
add(1, 4, (sum) => {
  console.log(sum); // Should print: 5
});
