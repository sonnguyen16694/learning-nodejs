const http = require("http");
const https = require("https");
const url_geo_api = `https://api.mapbox.com/geocoding/v5/mapbox.places/Hanoi.json?access_token=pk.eyJ1IjoidHV5a2llbXRpZW4iLCJhIjoiY2tqNzUzM3BlMWF6cDJwbnZ0OHM0NWRpcSJ9.TaVZ5F9_ZZthhwSmbu6eCQ`;

const request = https.request(url_geo_api, (response) => {
  let data = "";
  response.on("data", (chunk) => {
    data += chunk.toString();
  });
  response.on("end", () => {
    const body = JSON.parse(data);
    console.log(body);
  });
});

request.on("error", (error) => {
  console.log("An error", error);
});
request.end();
